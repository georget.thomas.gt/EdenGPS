import random #To create the gps coordinates file - Non necessary in final version
import csv # to read and realize operations on .csv files
import datetime #Timecode management
import time
##

def creation_csv(path): #(str)
    valeurs = []
    count = 0
    for t in range(1682226000,1682236800):
        if count%10000 == 0:
            print(count)
        for ms in range(0,1000,10):
            timecode = time.strftime("%Y-%m-%d_%H-%M-%S", time.gmtime(t))
            if ms > 99:
                s = str(ms)
            elif ms >9 :
                s = '0'+str(ms)
            else:
                s = '00'+str(ms)
            timecode = timecode+'-'+s

            lat = random.randint(-90, 90)+round(random.random(),7)
            long = random.randint(-180, 180)+round(random.random(),7)
            alt = random.randint(0,2500)

            valeurs.append([timecode, lat, long, alt])
        count+=1
    with open('coord_GPS.csv', 'w', newline='\n') as csvfile:
        spamwriter = csv.writer(csvfile)
        for valeur in valeurs:
            spamwriter.writerow(valeur)
    csvfile.close()