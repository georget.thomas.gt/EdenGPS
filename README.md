# EdenGPS
Le projet est un travail réalisé à la demande de l'entreprise EdenMap par Junior Atlantique. Toute utilisation nécessite l'accord de la partie proriétaire. 

Ce code a pour objectif d'ajouter les coordonnées GPS contenues dans un fichier au format csv aux images d'une vidéo au format mp4. 
L'association entre les coordonnées et les images se fait sur une correspondance des instants de mesurede la position et de prise de vue, à la milliseconde.
Pour pouvoir utiliser ce code, il est nécessaire d'installer diverses bibliothèques, recensées ci-dessous, avec les lignes de commande pour les installer (les librairies natives ne nécessitent pas de manipulations particulières) : 
- opencv / python -m pip install opencv-python
- os / native library
- csv / native library
- re / native library
- shutil / native library
- datetime / native library
- piexif / python -m pip install piexif
- tkinter / python -m pip install tk