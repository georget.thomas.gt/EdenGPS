import cv2 # video and image operations
import os # To manage the files
import csv # to read and realize operations on .csv files
import re # regular expressions, for tests
import shutil # to move files easily
import datetime #Timecode management
import piexif #To manage metadata of images
import tkinter as tk #To manage the GUI


## Path search

#TO MODIFY : input the real path to the file with all the files
o_path = "C:\\Users\\georg\\Documents\\Thomas\\Scolarite\\IMTA\\JA\\EdenGPS\\Test Chantrerie\\"
#"C:\Users\georg\Documents\Thomas\Scolarite\IMTA\JA\EdenGPS\EdenGPS\"

def find_path(path):
    path_candidates = [
        path,
        "./"
    ]
    for path_candidate in path_candidates:
        if os.path.exists(path_candidate):
            return path_candidate

    return path


path = find_path(o_path)
nom = "video.mp4"
directory = path
os.chdir(directory)


## function definition

# Datetime-string conversion
def str2date(s): #(str)
    return datetime.datetime.strptime(s, "%Y-%m-%d_%H-%M-%S-%f")
def date2str(d):
    return d.strftime("%Y-%m-%d_%H-%M-%S-%f")[0:-3]
def str2str(s):
    try:
        d = datetime.datetime.strptime(s, "%Y-%m-%d %H:%M:%S.%f")
        return d.strftime("%Y-%m-%d_%H-%M-%S-%f")[0:-3]
    except ValueError:
        d = datetime.datetime.strptime(s, "%Y-%m-%d %H:%M:%S")
        return d.strftime("%Y-%m-%d_%H-%M-%S-%f")[0:-3]


# Video to images handling
def conversion_image(nom, path,side, timecode, delta): #(str, str, str, datetime, timedelta)
    if side not in ['Right', 'right', 'Left', 'left', 'Droite', 'droite', 'Gauche', 'gauche']:
        side = ''
    vidcap = cv2.VideoCapture(path+nom)
    success,image = vidcap.read()
    nframe= vidcap.get(cv2.CAP_PROP_FRAME_COUNT)
    print('Nombre de frames total : ', nframe)
    count = 0

    timecode = timecode - nframe*delta
    while count < nframe:
        timesum = timecode+count*delta
        timestring = date2str(timesum)
        filename = side + 'I_' +timestring+'.jpg'
        if success == True:
            cv2.imwrite(filename, image)
        success,image = vidcap.read()
        # Print a counter to highlight the progress
        if count%100 == 0:
            print('Read new frames: ', success, count)
        count += 1
        if success ==False:
            print(success, count)



    vidcap.release()
    print("Conversion en images finie")
    return nframe


def moving_image(path, L_end): #(str)
    files = []
    for file in os.listdir(): # Image file identification
        test = re.search('(.)+.jpg', file)
        if test is not None:
            files.append(file)
    dest = path+date2str(L_end)[0:-7]+"_SavedImages\\"
    if not os.path.exists(dest): #creation of destination file
        os.makedirs(dest)
    for file in files : # copy and remove of images file
        chemin_origine = os.path.join(path, file)
        chemin_dest = os.path.join(dest, file)
        shutil.copy2(file, dest)
        os.remove(file)



def get_video_datetime(path, file): #(str, str)
    chemin_video = path + file
    date_modif = os.path.getmtime(chemin_video)
    date_modification_readable =  datetime.datetime.fromtimestamp(date_modif)
    return datetime.datetime.fromtimestamp(date_modif)



#Coordinates conversion from deg to triplets (°,',")
def deg_to_dms(deg): #(float)
    d = int(deg)
    m = abs(int((deg - d) * 60))
    s = abs(int(((deg - d) * 60 - m) * 60))
    return (d, 1), (m, 1), (s, 1)

def dms_to_deg(dms, ref):
    deg = dms[0][0]+dms[1][0]/60+dms[2][0]/3600
    if ref == 'S' or ref =='W':
        return -deg
    else :
        return deg


# Coordinates addition for 1 picture
def add_coord_photo(nom, alt, lat, long): #(str, str, str, str)
    latitude = float(lat)
    longitude = float(long)
    filename = nom
    exif_dict = piexif.load(filename)
    gps_ifd = {
    piexif.GPSIFD.GPSLatitudeRef: 'N' if latitude >= 0 else 'S',
    piexif.GPSIFD.GPSLatitude: deg_to_dms(abs(latitude)),
    piexif.GPSIFD.GPSLongitudeRef: 'E' if longitude >= 0 else 'W',
    piexif.GPSIFD.GPSLongitude: deg_to_dms(abs(longitude)),
    }
    exif_dict['GPS'] = gps_ifd
    exif_bytes = piexif.dump(exif_dict)
    piexif.insert(exif_bytes, filename)


# Coordinates addition for all pictures in a file
def add_coord(d): #(dict)
    for file in os.listdir():
        if file[-4:] == '.jpg':
            try:
                date = file[-27:-8]
                coord = d[date]
                add_coord_photo(file, coord[2], coord[0], coord[1])
            except KeyError as e :
                print(file)
                pass

# Timecode addition for 1 picture
def add_datetime_photo(nom, timecode): #(str, datetime.datetime)
    filename = nom
    exif_dict = piexif.load(filename)
    new_date = timecode.strftime("%Y:%m:%d %H:%M:%S")
    exif_dict['0th'][piexif.ImageIFD.DateTime] = new_date
    exif_dict['Exif'][piexif.ExifIFD.DateTimeOriginal] = new_date
    exif_dict['Exif'][piexif.ExifIFD.DateTimeDigitized] = new_date
    exif_bytes = piexif.dump(exif_dict)
    piexif.insert(exif_bytes, filename)

# Timecode addition for all pictures in a file
def add_datetime():
    for file in os.listdir():
        if file[-4:]=='.jpg':
            add_datetime_photo(file, str2date(file[-27:-4]))


# Data recuperation from csv
def lecture_csv(nom): #(str)
    with open(path + nom, newline='\n') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',')
        L=[]
        for row in spamreader :
            L.append(row)
    return L

# Conversion from list to dict
def conv_gps_dict(list):
    #[type,datetime,latitude,longitude,accuracy(m),altitude(m),geoid_height(m),speed(m/s),bearing(deg),sat_used,sat_inview,name,desc] --> {'datetime' : ['lat','long','alt']
    d={}
    for k in list[1:] :
        timecode = str2str(k[1])
        d[timecode[:-4]]= [k[i] for i in range(2,11)]

    return d


# Creation of the image-coordinates correspondance csv from a file
def ecriture_csv(path, dict): #(str, dict)
    entetes = [
        'Timecode',
        'Nom_image',
        'Latitude',
        'Longitude',
        'Accuracy',
        'Altitude',
        'Geoid height',
        'Speed',
        'bearing',
        'Satellites used',
        'Satellites in view'
    ]
    valeurs = []
    for file in os.listdir():
        if file[-4:] == '.jpg':
            try :
                timecode = file[-27:-8]
                nom_image = file
                lat = dict[timecode][0]
                long = dict[timecode][1]
                acc = dict[timecode][2]
                alt = dict[timecode][3]
                g_height = dict[timecode][4]
                speed = dict[timecode][5]
                bearing = dict[timecode][6]
                sat_used = dict[timecode][7]
                sat_inview = dict[timecode][8]
                valeurs.append([timecode, nom_image, lat, long, acc, alt, g_height, speed, bearing, sat_used, sat_inview])
            except KeyError as e:
                print (e)

    with open('correspondance_GPS_png.csv', 'w', newline='\n') as csvfile:
        spamwriter = csv.writer(csvfile)
        spamwriter.writerow(entetes)
        for valeur in valeurs:
            spamwriter.writerow(valeur)
    csvfile.close()


def question(questions): #(list)
    # Fonction appelée lorsque l'utilisateur clique sur le bouton "OK" ou appuie sur la touche "Entrée"
    def get_user_response(event=None):
        global current_question_index
        response = entry.get()  # Récupérer la réponse de l'utilisateur depuis le champ de saisie
        responses.append(response)  # Ajouter la réponse à la liste de réponses
        entry.delete(0, tk.END)  # Effacer le contenu du champ de saisie
        # Passer à la question suivante ou fermer la fenêtre si toutes les questions ont été posées
        if current_question_index < len(questions) - 1:
            current_question_index += 1
            label.config(text=questions[current_question_index])
        else:
            window.destroy()

    # Créer la fenêtre principale
    window = tk.Tk()

    # Liste des réponses
    responses = []

    # Index de la question actuelle
    current_question_index = 0

    # Créer un label pour afficher les questions
    label = tk.Label(window, text=questions[current_question_index])
    label.pack()

    # Créer un champ de saisie
    entry = tk.Entry(window)
    entry.pack()

    # Créer un bouton "OK" pour valider la réponse
    button = tk.Button(window, text="OK", command=get_user_response)
    button.pack()

    # Lier l'événement "Retour" (touche Entrée) à la fonction get_user_response
    window.bind("<Return>", get_user_response)

    # Lancer la boucle principale de la fenêtre
    window.mainloop()

    # Afficher les réponses
    return responses


##
path = o_path
os.chdir(path)
if __name__ == "__main__":
    # Liste des questions
    current_question_index = 0
    questions = [
        "Quel est le dossier regroupant les différents fichiers ? (Entrer l\'arborescence complète avec des \\\)",
        "Quel est le nom du fichier GPS ? (format .csv)",
        "Quel est le nom du fichier de la caméra droite ?",
        "Quel est le nom du fichier de la caméra gauche ?",
        "Quel est l'intervalle entre 2 prises de vues ? (en secondes)"
    ]
    reponses = question(questions)

    #path recuperation and verification
    path = reponses[0]
    path = find_path(path)
    print(path)
    if not os.path.exists(path):
        raise TypeError("Le dossier n'existe pas'")
    directory = path
    os.chdir(directory)

    # Files name recuperation and verification of file's format
    nom_GPS = reponses[1]
    if nom_GPS == '':
        raise TypeError("Le nom du fichier GPS est invalide.")
    test1 = re.search('(.)+\.csv', nom_GPS)
    test2 = re.search('(.)+\.txt', nom_GPS)
    if test1 is None and test2 is None :
        raise TypeError("Le format du fichier GPS est invalide.")

    nom_cam_droite=reponses[2]
    if nom_cam_droite == '':
        raise TypeError("Le nom du fichier vidéo de la caméra droite est invalide.")
    test1 = re.search('(.)+\.mp4', nom_cam_droite)
    test2 = re.search('(.)+\.MP4', nom_cam_droite)
    if test1 is None and test2 is None :
        raise TypeError("Le format du fichier vidéo de la caméra droite est invalide.")

    nom_cam_gauche=reponses[3]
    if nom_cam_gauche == '':
        raise TypeError("Le nom du fichier vidéo de la caméra gauche est invalide.")
    test1 = re.search('(.)+\.mp4', nom_cam_gauche)
    test2 = re.search('(.)+\.MP4', nom_cam_gauche)

    if test1 is None and test2 is None:
        raise TypeError("Le format du fichier vidéo de la caméra gauche est invalide.")

    delta = reponses[4]
    if delta == '':
        raise TypeError("L\'intervalle précisé est invalide.")
    tdelta = datetime.timedelta(seconds = float(delta))

    #Videos' start time recuperation
    Left_vid_end = get_video_datetime(path, nom_cam_gauche)
    Right_vid_end = get_video_datetime(path, nom_cam_droite)


    print("Conversion en image du fichier vidéo droit...")
    conversion_image(nom_cam_droite, path, "Right", Right_vid_end, tdelta)
    print("Conversion en image du fichier vidéo gauche...")
    conversion_image(nom_cam_gauche, path, "Left", Left_vid_end, tdelta)
    print("Déplacement des images dans un sous-dossier")
    moving_image(path, Left_vid_end)

    print("Récupération et traitement des données GPS...")
    list = lecture_csv(nom_GPS)
    dict = conv_gps_dict(list)

    print("Association des dates aux images...")
    subfolder = date2str(Left_vid_end)[0:-7]+"_SavedImages\\"
    new_path = path + subfolder #Repertory change to work on saved images
    directory = new_path
    os.chdir(directory)
    add_datetime()
    print("Ajout des dates en métadonnées finies")

    print("Association des coordonnées GPS aux images...")
    add_coord(dict)
    print("Fin de l'ajout des coordonnées GPS")

    print("Ecriture du nouveau fichier csv de correspondance image-coordonnées")
    ecriture_csv(path, dict)
    print("Fin du script")



##


#15/06 : création de la fonction de conversion vidéo - images
#17/06 : création de la fonction pour déplacer les images
        #création des fonction de récupération et convertion des coordonnées GPS
        #création des fonction de récupération des instants de début des vidéos
        #création des débuts de fonctions pour ajouter les métadonnées aux images
    #Reste à faire : ajouter les coordonnnées GPS  aux photos, vérifier que toutes les fonctions marchent
#18/06 : nouveau format de nom pour pouvoir associer les ms à l'image, impossible sans cela de les associer dans les timestamps
        #association des métadonnées temporelles aux images(exif)
#19/06 : création de la fonction d'ajout des coordonnées GPS aux exif images - ATTENTION, création d'un dictionnaire aléatoire pour le test dans le script !!!
        #création de la fonction d'écriture du csv de correspondance coordonnées-images
#20/06 : Correction de la gestion des coordonnées pour un calcul correct
        #Ajout de commentaires détaillant chacune des fonctions
#21/06 : Tests et débuggage
#22/06 : ajout de l'IHM permettant de récupérer les noms de fichiers
#29/06 : Correction d'une erreur de traitement sur les coordonnées négatives
#15/07 : Ajout de champs de donnée dans le csv récapitulatif
#24/07 : Gestion de l'exception des secondes précises
        # Correction du décalage temporel du GPS

#14/09/2023 : Changer le sous-dossiers pour mettre le timecode --> garder le script à un endroit


